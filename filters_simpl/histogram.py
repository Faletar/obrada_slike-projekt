import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


def imhist(im):
  # calculates histogram of an image
	m, n = im.shape
	h = [0.0] * 256  # h -> histogram
	for i in range(m):
		for j in range(n):
			h[im[i, j]]+=1
	return np.array(h)/(m*n)

 # convert image to grayscale
img = np.uint8(mpimg.imread('mid-tones.png')*255.0)  #na mjesto gdje piše mid-tones.png se stavlja naziv slike koja se želi pretvoriti u grayscale odnosno histogram

# do for individual channels R, G, B, A for nongrayscale images
img = np.uint8((0.2126* img[:,:,0]) + \
  		np.uint8(0.7152 * img[:,:,1]) +\
			 np.uint8(0.0722 * img[:,:,2]))
			 
h = imhist(img)

# show grayscale image
plt.imshow(img)
plt.title('Gray image') # grayscale
plt.set_cmap('gray')

# plot histogram
fig = plt.figure()
plt.plot(h)
plt.title('Histogram') # histogram

plt.show()